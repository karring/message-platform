package com.joysuccess.bacnet.controller;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import com.alibaba.fastjson.JSONObject;
import com.joysuccess.common.utils.HelpUtils;
import com.joysuccess.common.utils.ResponseJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author : zhangqing
 * @Description: 在线修改日志级别
 * @date : 2020年06月02日 17:08
 */
@RestController
@RequestMapping("/loglevel")
@Api(value = "OnLineUpdateLogLevelController",description = "在线修改日志级别")
public class OnLineUpdateLogLevelController {

    private final static int CHANGE_LOG_STATUS = 200;
    private  static String response_msg = "修改日志等级为：";
    private  static String ROOT_LEVEL_STR = "root";

    @RequestMapping(value = "/root/info", method = RequestMethod.GET)
    @ApiOperation(value = "修改日志级别为info", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseJson logLevelToInfo() {
        updateLogLevel(Level.INFO,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.INFO.levelStr);
    }

    @ApiOperation(value = "修改日志级别为debug", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/root/debug", method = RequestMethod.GET)
    public ResponseJson logLevelToDebug() {
        updateLogLevel(Level.DEBUG,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.DEBUG.levelStr);
    }

    @ApiOperation(value = "修改日志级别为warn", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/root/warn", method = RequestMethod.GET)
    public ResponseJson logLevelToWarn() {
        updateLogLevel(Level.WARN,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.WARN.levelStr);
    }

    @ApiOperation(value = "修改日志级别为trace", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/root/trace", method = RequestMethod.GET)
    public ResponseJson logLevelToTrace() {
        updateLogLevel(Level.TRACE,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.TRACE.levelStr);
    }

    @ApiOperation(value = "修改日志级别为error", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/root/error", method = RequestMethod.GET)
    public ResponseJson logLevelToError() {
        updateLogLevel(Level.ERROR,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.ERROR.levelStr);
    }

    @ApiOperation(value = "修改日志级别为off", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/root/off", method = RequestMethod.GET)
    public ResponseJson logLevelToOff() {
        updateLogLevel(Level.OFF,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.OFF.levelStr);
    }

    @ApiOperation(value = "修改日志级别为all", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/root/all", method = RequestMethod.GET)
    public ResponseJson logLevelToAll() {
        updateLogLevel(Level.ALL,ROOT_LEVEL_STR);
        return new ResponseJson(CHANGE_LOG_STATUS,response_msg + Level.ALL.levelStr);
    }

    /**
     * 根据日志类型修改日志级别
     * @param level
     * @param type
     */
    public void updateLogLevel(Level level,String type){
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger logger = loggerContext.getLogger(type);
        logger.setLevel(level);
    }

    /**
     * 获取日志类型，如果没有自定义其它类型，则就是Root
     * @return
     */
    @ApiOperation(value = "获取日志类型", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/types", method = RequestMethod.GET)
    public ResponseJson logLevelTypes() {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        List<Logger> loggerList = loggerContext.getLoggerList();

        JSONObject jsonObject = new JSONObject();
        if(HelpUtils.isNotEmpty(loggerList)){
            for(Logger logger : loggerList){
                jsonObject.put(logger.getName(),logger.getLevel());
            }
        }
        return new ResponseJson(CHANGE_LOG_STATUS,null,jsonObject.toJSONString());
    }
}

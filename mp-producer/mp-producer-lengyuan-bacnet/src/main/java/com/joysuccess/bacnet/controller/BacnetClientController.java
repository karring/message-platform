package com.joysuccess.bacnet.controller;

import com.joysuccess.common.utils.ResponseJson;
import com.joysuccess.bacnet.service.BacnetClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangqing
 * 冷源群控控制器：MySQL初始化数据，Redis数据初始化，Kafka数据发送等接口
 */
@RestController
@RequestMapping("/bacnet")
@Api("冷源群控数据操作接口")
public class BacnetClientController {

    @Autowired
    private BacnetClientService bacnetService;
    /**
     * 操作成功
     */
    private final static int BACNET_SUCCESS = 200;
    private final static String BACNET_SUCCESS_MSG = "操作成功";
    /**
     * 操作成功
     */
    private final static int BACNET_ERROR = 500;
    private final static String BACNET_ERROR_MSG = "操作失败";

//    /**
//     * 冷源群控数据全量导入MySQL中
//     * @return
//     */
//    @ApiOperation(value = "冷源群控数据全量 -> MySQL初始化", notes="冷源群控数据全量 -> MySQL初始化")
//    @GetMapping("/init/all/to/mysql")
//    public ResponseJson initMibsToMySQL() {
//        try {
//            bacnetService.initBacnetDataToMySQL();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseJson(BACNET_ERROR,BACNET_ERROR_MSG + e.getMessage(),null);
//        }
//        return new ResponseJson(BACNET_SUCCESS,BACNET_SUCCESS_MSG,null);
//    }

//    /**
//     * 冷源群控数据从MySQL初始化到Redis中
//     * @return
//     */
//    @ApiOperation(value = "冷源群控数据 MySQL -> Redis初始化", notes="冷源群控数据MySQL -> Redis初始化")
//    @GetMapping("/init/redis/from/mysql")
//    public ResponseJson initRedisFromMysql() {
//        bacnetService.initRedisFromMySQLOnceAll();
//        return new ResponseJson(BACNET_SUCCESS,BACNET_SUCCESS_MSG);
//    }

    /**
     * 获取所有冷源群控数据发送到kafka中
     * @return
     */
    @ApiOperation(value = "冷源群控数据 全量 -> Kafka 一次", notes="冷源群控数据 全量 -> Kafka 一次")
    @GetMapping("/insert/all/to/kafka")
    public ResponseJson setAllBacnetMibsToKafka() {
        try {
            bacnetService.initBacnetDataToKafka();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseJson(BACNET_ERROR,e.getMessage(),null);
        }
        return new ResponseJson(BACNET_SUCCESS,BACNET_SUCCESS_MSG,null);
    }
}

package com.joysuccess.bacnet.sources.mysql.model;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName( "sm_collection_point")
public class SmCollectionPoint {
    /**
     * 活跃的，及没有删除的记录
     */
    public static final Integer ACTIVE_FLAG_YES = 1;

    /**
     * 不活跃的，及删除的记录
     */
    public static final Integer ACTIVE_FLAG_NO = 0;

    /**
     * 主键
     */
    @TableId("id")
    protected String id;

    /**
     * 采集点名称
     */
    @TableField( "POINT_ORIGIN")
    private String pointOrigin;

    /**
     * 采集点名称
     */
    @TableField( "POINT_NAME")
    private String pointName;

    /**
     * 采集点编码'
     */
    @TableField("POINT_CODE")
    private String pointCode;

    /**
     * 类型
     */
    @TableField("POINT_TYPE")
    private String pointType;

    /**
     * 阀值上限
     */
    @TableField("THRESHOLD_UP")
    private Double thresholdUp;

    /**
     * 阀值下限
     */
    @TableField("THRESHOLD_DOWN")
    private Double thresholdDown;

    /**
     * 阀值
     */
    @TableField("THRESHOLD")
    private Integer threshold;

    /**
     * 采集值
     */
    @TableField("POINT_VALUE")
    private String pointValue;

    /**
     * 采集时间
     */
    @TableField("POINT_DATE")
    private Date pointDate;

    /**
     * 是否告警
     */
    @TableField("IS_ALARM")
    private Integer isAlarm;

    /**
     * 排版序号
     */
    @TableField("SERIAL_NUMBER")
    private Integer serialNumber;

    /**
     * 所属机房
     */
    @TableField("ROOM_ID")
    private String roomId;

    /**
     * 所在设备
     */
    @TableField("ASSERT_ID")
    private String assertId;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 动态环境监控关系映射
     */
    @TableField("dynamic_world_map")
    private String dynamicWorldMap;

    /**
     * 采集点简称
     */
    @TableField(  "coll_point_name_short")
    private String collPointNameShort;

    /**
     * 漏水绳图
     */
    @TableField( "units")
    private String units;

    /**
     * 断路器
     */
    @TableField( "circuit_breaker_id")
    private String circuitBreakerId;

    /** */
    @TableField("CREATE_BY")
    private String createBy;

    /** */
    @TableField("ACTIVE_FLAG")
    private Integer activeFlag;

    /** */
    @TableField("UPDATE_BY")
    private String updateBy;

    /** */
    @TableField("CREATE_DATE")
    private Date createDate;

    /** */
    @TableField("UPDATE_DATE")
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getPointName() {
        return pointName;
    }

    public void setPointName(String pointName) {
        this.pointName = pointName == null ? null : pointName.trim();
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode == null ? null : pointCode.trim();
    }

    public String getPointType() {
        return pointType;
    }

    public void setPointType(String pointType) {
        this.pointType = pointType == null ? null : pointType.trim();
    }

    public Double getThresholdUp() {
        return thresholdUp;
    }

    public void setThresholdUp(Double thresholdUp) {
        this.thresholdUp = thresholdUp;
    }

    public Double getThresholdDown() {
        return thresholdDown;
    }

    public void setThresholdDown(Double thresholdDown) {
        this.thresholdDown = thresholdDown;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public String getPointValue() {
        return pointValue;
    }

    public void setPointValue(String pointValue) {
        this.pointValue = pointValue == null ? null : pointValue.trim();
    }

    public Date getPointDate() {
        return pointDate;
    }

    public void setPointDate(Date pointDate) {
        this.pointDate = pointDate;
    }

    public Integer getIsAlarm() {
        return isAlarm;
    }

    public void setIsAlarm(Integer isAlarm) {
        this.isAlarm = isAlarm;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getRoomId() {
        return roomId;
    }


    public void setRoomId(String roomId) {
        this.roomId = roomId == null ? null : roomId.trim();
    }

    public String getAssertId() {
        return assertId;
    }

    public void setAssertId(String assertId) {
        this.assertId = assertId == null ? null : assertId.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getDynamicWorldMap() {
        return dynamicWorldMap;
    }

    public void setDynamicWorldMap(String dynamicWorldMap) {
        this.dynamicWorldMap = dynamicWorldMap == null ? null : dynamicWorldMap.trim();
    }

    public String getCollPointNameShort() {
        return collPointNameShort;
    }

    public void setCollPointNameShort(String collPointNameShort) {
        this.collPointNameShort = collPointNameShort == null ? null : collPointNameShort.trim();
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units == null ? null : units.trim();
    }

    public String getCircuitBreakerId() {
        return circuitBreakerId;
    }

    public void setCircuitBreakerId(String circuitBreakerId) {
        this.circuitBreakerId = circuitBreakerId == null ? null : circuitBreakerId.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Integer getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(Integer activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }


    public String getPointOrigin() {
        return pointOrigin;
    }

    public void setPointOrigin(String pointOrigin) {
        this.pointOrigin = pointOrigin;
    }

}
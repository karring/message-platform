package com.joysuccess.bacnet.sources.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joysuccess.bacnet.sources.mysql.model.SmCollectionPoint;


public interface CollectionPointMapper extends BaseMapper<SmCollectionPoint> {

}

package com.joysuccess.bacnet.service;

import com.serotonin.bacnet4j.exception.BACnetException;

/**
 * @author : zhangqing
 * @Description: 冷源群控相关接口
 * @date : 2020年06月03日 10:51
 */
public interface BacnetClientService {
    /**
     * 冷源群控数据：从MySQL初始化到Redis中
     */
//    public boolean initRedisFromMySQLOnceAll();
    /**
     * 获取BAcnet所有数据，并将数据发送到kafka中
     * @return Object
     * @throws BACnetException
     * @throws Exception
     */
    public void initBacnetDataToKafka() throws BACnetException, Exception;

    /**
     * 初始化冷源群控数据--->MySQL
     */
//    public void initBacnetDataToMySQL() throws Exception;
}

package com.joysuccess.bacnet;

import com.joysuccess.bacnet.service.BacnetClientService;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhangqing
 */
@SpringBootApplication
@MapperScan("com.joysuccess.bacnet.sources.mysql.mapper")
public class BacnetClientApplication implements ApplicationRunner {
    @Autowired
    private BacnetClientService bacnetService;

    private final static Logger LOGGER = LoggerFactory.getLogger(BacnetClientApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BacnetClientApplication.class,args);
    }

    /**
     * 启动成功之后，执行的代码
     * @param args
     */
    @Override
    public void run(ApplicationArguments args) {
        /**
         * 创建一个单独的线程做循环获取BAcnet数据，并发送到Kafka中
         */
        LOGGER.info("BAcnetProducerThread线程开启--->准备循环获取Bacnet数据，并发送到Kafka中");
        new Thread(()->{
            /**
             * 在项目启动的时候，从MySQL中初始化Redis的数据
             */
//            boolean isSuccess =false;
            for(;;){
                try {
                    bacnetService.initBacnetDataToKafka();
                } catch (Exception e) {
                    LOGGER.info("发送数据到Kafka异常--------------");
                    e.printStackTrace();
                }
//                if(!isSuccess){
//                    LOGGER.info("准备初始化数据到Redis....");
//                    isSuccess = bacnetService.initRedisFromMySQLOnceAll();
//                   if(isSuccess) {LOGGER.info("Redis数据初始化成功");}
//                }
//                if(isSuccess){
//                    try {
//                        LOGGER.info("准备从冷源群控系统中拉取数据到Kafka中");
//                        bacnetService.initBacnetDataToKafka();
//                    } catch (Exception e) {
//                        LOGGER.info("发送数据到Kafka异常--------------");
//                        e.printStackTrace();
//
//                    }
//
//                }else {
//                    LOGGER.info("初始化Redis数据失败，MySQL中没有数据，等待手动初始化MySQL中的数据");
//                }
                try {
                    //30秒轮训一次
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"BAcnetProducerThread").start();
    }
}

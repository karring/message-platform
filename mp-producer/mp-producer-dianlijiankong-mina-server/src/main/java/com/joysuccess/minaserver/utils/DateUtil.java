package com.joysuccess.minaserver.utils;

import com.joysuccess.common.utils.HelpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 日期工具类 java对日期的操作一直都很不理想,直到jdk1.8之后才有了本质的改变。
 * 如果使用的是jdk1.8之前版本开发的话,推荐使用Joda Time组件。
 * <p>
 * 特别备注:unix的时间戳单位是秒
 *
 * @author shenchao
 **/
public class DateUtil {
    private static Logger log = LoggerFactory.getLogger(DateUtil.class);

    /**
     * 年-月-日 时:分:秒 显示格式
     */
    // 备注:如果使用大写HH标识使用24小时显示格式,如果使用小写hh就表示使用12小时制格式。
    public static String DATE_TO_STRING_DETAIAL_PATTERN = "yyyy-MM-dd HH:mm:ss";

    /**
     * 年-月-日 显示格式
     */
    public static String DATE_TO_STRING_SHORT_PATTERN = "yyyy-MM-dd";

    private static SimpleDateFormat simpleDateFormat;

    /**
     * Date类型转为指定格式的DateString类型
     *
     * @param date
     * @param pattern
     * @return
     */
    public static String date2DateStr(Date date, String pattern) {
        if (HelpUtils.isEmpty(pattern)) {
            pattern = DATE_TO_STRING_DETAIAL_PATTERN;
        }
        simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    /**
     * 指定格式的DateStr类型转为Date类型
     *
     * @param dateStr
     * @param pattern
     * @return
     */
    public static Date dateStr2Date(String dateStr, String pattern) {
        if (HelpUtils.isEmpty(pattern)) {
            pattern = DATE_TO_STRING_DETAIAL_PATTERN;
        }
        simpleDateFormat = new SimpleDateFormat(pattern);
        try {
            return simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    /**
     * 将日期转换为时间戳(unix时间戳,单位秒)
     *
     * @param date
     * @return
     */
    public static long date2TimeStamp(Date date) {
        Timestamp timestamp = new Timestamp(date.getTime());
        return timestamp.getTime() / 1000;

    }


    /**
     * Java将Unix时间戳转换成指定格式日期字符串
     *
     * @param timeStamp 时间戳 如："1473048265";
     * @param formats   要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static String timeStamp2DateStr(Long timeStamp, String formats) {
        if (HelpUtils.isEmpty(formats)) {
            formats = DATE_TO_STRING_DETAIAL_PATTERN;
        }
        int length = timeStamp.toString().length();
        if (length < 13) {
            int diff = 13 - length;
            timeStamp = timeStamp * ((long) Math.pow(10, diff));
        }
        String date = new SimpleDateFormat(formats, Locale.CHINA).format(new Date(timeStamp));
        return date;
    }

    /**
     * Java将Unix时间戳字符串转换成指定格式日期字符串
     *
     * @param timeStampStr 时间戳 如："1473048265";
     * @param formats      要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static String timeStampStr2DateStr(String timeStampStr, String formats) {
        if (HelpUtils.isNotEmpty(timeStampStr)) {
            timeStampStr = timeStampStr.trim();
        }
        Long timestamp = Long.parseLong(timeStampStr);
        String date = timeStamp2DateStr(timestamp, formats);
        return date;
    }

    /**
     * Java将Unix时间戳字符串转换成指定格式日期
     *
     * @param timeStampStr 时间戳 如："1473048265";
     * @param formats      要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static Date timeStampStr2Date(String timeStampStr, String formats) {
        Long timeStamp = Long.parseLong(timeStampStr.trim());
        return timeStamp2Date(timeStamp, formats);
    }

    /**
     * Java将Unix时间戳转换成指定格式日期
     *
     * @param timeStamp 时间戳 如：1473048265;
     * @param formats   要格式化的格式 默认："yyyy-MM-dd HH:mm:ss";
     * @return 返回结果 如："2016-09-05 16:06:42";
     */
    public static Date timeStamp2Date(Long timeStamp, String formats) {
        if (HelpUtils.isEmpty(formats)) {
            formats = DATE_TO_STRING_DETAIAL_PATTERN;
        }
        String dateString = timeStamp2DateStr(timeStamp, formats);
        SimpleDateFormat format = new SimpleDateFormat(formats);
        Date date = null;
        try {
            date = format.parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }


    /**
     * 获得当前时间对应的指定格式DateString
     *
     * @param pattern
     * @return
     */
    public static String getCurrentDateStr(String pattern) {
        if (HelpUtils.isEmpty(pattern)) {
            pattern = DATE_TO_STRING_DETAIAL_PATTERN;
        }
        simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(new Date());

    }

    /**
     * 获得当前unix时间戳(单位秒)
     *
     * @return 当前unix时间戳
     */
    public static long getCurrentUnixTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }

    public static void main(String[] args) {
        String timeStampString = "1514875795123";
        Long timeStamp = 1514875795999L;
        System.out.println(timeStamp2Date(timeStamp, null));

    }
}
//待补充

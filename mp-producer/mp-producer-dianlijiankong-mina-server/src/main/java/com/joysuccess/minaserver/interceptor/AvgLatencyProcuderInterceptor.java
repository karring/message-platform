package com.joysuccess.minaserver.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * 生产者拦截器,主要是统计计算生产消息和消费消息每条处理的时间延迟
 */
@Slf4j
public class AvgLatencyProcuderInterceptor implements ProducerInterceptor<String,String> {

    private Jedis jedis = new Jedis("127.0.0.1",6379);

    @Override
    public ProducerRecord onSend(ProducerRecord<String,String> producerRecord) {
        //计算总数据大小
        try {
            jedis.incr("totalSentMessage");
        }catch (Exception e){
            log.info("error: AvgLatencyProcuderInterceptor.onSend() msg:"+e.getMessage());
        }

        return producerRecord;
    }

    @Override
    public void onAcknowledgement(RecordMetadata recordMetadata, Exception e) {

    }

    @Override
    public void close() {

    }

    @Override
    public void configure(Map<String, ?> map) {

    }
}

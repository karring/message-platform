package com.joysuccess.minaserver.minaserver.raw;

/**
 * 指令枚举
 */
public enum Ins {
    DATA("数据","1"),ALARM("告警","10"),REQUEST("请求","11"),RESPONSE("回复","100");
    private String means;
    private String value;

    Ins(String means, String value) {
        this.means = means;
        this.value = value;
    }

    public static String getMeans(String value) {
        for (Ins ins : Ins.values()) {
            if (ins.getValue().equals(value)) {
                return ins.means;
            }
        }
        return null;
    }

    public String getMeans() {
        return means;
    }

    public void setMeans(String means) {
        this.means = means;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

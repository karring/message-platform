package com.joysuccess.minaserver.constant;

/**
 * kafka消息主题名称常量
 */
public class TopicName {
    /**
     * 电力监控的消息主题，来源为：
     * 通过MinaServer的ServerHandler中，获取sokect客户端发送过来的请求数据，DCIM系统为服务端（MinaServer），厂商为客户端（socket-client）
     */
    public final static String DLJK_DATA_TOPIC_0001="DLJK-COL-MSG-DATA-0001";

    /**
     * 所有告警的消息主题，所有厂商告警统一输出到当前的消息主题，告警的数据量不是太大，分散为各个小的主题，维护量将会很大
     * 此时会有个问题，就是可能会缺少告警的来源，所以需要一个告警的来源维护
     * @see AlarmResource
     */
    public final static String ALL_MSG_COL_ALARM="alarm-messages";



}

package com.joysuccess.minaserver.utils;


import com.joysuccess.common.utils.HelpUtils;

/**
 * 数字工具类
 * @author 孙少平
 * @date 2016年4月25日
 */
public abstract class MathUtils extends HelpUtils {

	/**
	 * 参数 “one”，“two”，“three” 其中一个为“null”返回empty，
	 * true： one < two < three,否则返回false
	 * @author 孙少平
	 * @date 2016年4月25日	
	 * @param one 
	 * @param two
	 * @param three
	 * @param empty
	 * @return
	 */
	public static boolean betweenNum(Number one, Number two, Number three, boolean empty) {
		if (isEmpty(one) || isEmpty(two) || isEmpty(three)) {
			return empty;
		}
		return two.doubleValue() > one.doubleValue() && two.doubleValue() < three.doubleValue();
	}

	/**
	 * 判断current是否在【min，max】这个区间内
	 * Created by heliyuan on 2016/11/3.
	 */
	public static boolean rangeInDefined(Double current, Double min, Double max)
	{
		return Math.max(min, current) == Math.min(current, max);
	}
}

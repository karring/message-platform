package com.joysuccess.minaserver.minaserver;

import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.net.InetSocketAddress;


public class MinaServer{

//    @Value("${minaserver.port}")
//    private Integer port;

    public void run(Integer port) throws Exception {
        // 4步操作
        // 1.新建 NioSocketAcceptor 事例对象
        NioSocketAcceptor acceptor = new NioSocketAcceptor();
        // 2.设置消息处理对象
        acceptor.setHandler(new ServerHandler());
        // 3.设置消息解码器
        acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ByteArrayCodecFactory()));// acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 3);
        // //设置idle时长
        // 4.绑定端口开启服务
        acceptor.getSessionConfig().setReceiveBufferSize(1024*1024);   // 设置接收缓冲区的大小
        acceptor.getSessionConfig().setSendBufferSize(1024*1024);// 设置输出缓冲区的大小
        acceptor.bind(new InetSocketAddress(port));
    }
}
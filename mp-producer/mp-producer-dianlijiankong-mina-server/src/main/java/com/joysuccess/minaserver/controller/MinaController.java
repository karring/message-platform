package com.joysuccess.minaserver.controller;

import com.joysuccess.common.utils.ResponseJson;
import com.joysuccess.minaserver.service.MinaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * snmp协议控制器
 * @author zhangqing
 * @date 2019-12-12
 */
@RestController
@RequestMapping("/mina")
@Api(value = "MinaController" ,description = "电力实时数据控制器")
public class MinaController {

    private final static Integer INIT_REDIS_STATUS = 200;
    private final static String INIT_REDIS_FAIL = "初始化Redis失败";
    private final static String INIT_REDIS_SUCCESS = "初始化Redis成功";

    @Autowired
    private MinaService snmpService;

    /**
     * 全量初始化Redis数据
     * 从MySQL中获取测点数据，并将数据初始化到Redis中
     * @return
     */
    @ApiOperation(value = "手动全量初始化Redis一次,测点数据源来自从MySQL查询数据")
    @PostMapping("/init-redis-from-mysql-once-all")
    public ResponseJson initSnmpDatasToRedisFromMySQL() {
       boolean isSuccess = snmpService.initRedisFromMysql();
       return new ResponseJson(INIT_REDIS_STATUS,isSuccess==true ? INIT_REDIS_SUCCESS : INIT_REDIS_FAIL);
    }
}

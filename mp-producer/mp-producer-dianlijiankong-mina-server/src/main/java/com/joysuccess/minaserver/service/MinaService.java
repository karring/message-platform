package com.joysuccess.minaserver.service;

/**
 * @author : yaowenlong
 * @Description: mina相关接口
 * @date : 2020年06月13日 14:25
 */
public interface MinaService {

    public boolean initRedisFromMysql();

}

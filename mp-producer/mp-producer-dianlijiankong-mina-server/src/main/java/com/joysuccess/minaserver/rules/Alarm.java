package com.joysuccess.minaserver.rules;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

/**
 * @author : zhangqing
 * @Description: 告警信息对象
 * @date : 2020年05月28日 12:40
 */
public class Alarm{
    private String alarmIndex;
    private String equipId;
    private String pointId;
    private Integer alarmsStatus;
    private Integer alarmLevel;
    private Date alarmTime;
    private String currentValue;
    private String alarmDescription;
    private Integer alarmConfirmStatus;

    public String alarmToJsonString(Alarm alarm){
        return JSONObject.toJSONString(alarm);
    }
    public Alarm jsonStringToAlarm(String jsonStr){
        return JSONObject.parseObject(jsonStr,Alarm.class);
    }

    public String getAlarmIndex() {
        return alarmIndex;
    }

    public void setAlarmIndex(String alarmIndex) {
        this.alarmIndex = alarmIndex;
    }

    public String getEquipId() {
        return equipId;
    }

    public void setEquipId(String equipId) {
        this.equipId = equipId;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }


    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getAlarmDescription() {
        return alarmDescription;
    }

    public void setAlarmDescription(String alarmDescription) {
        this.alarmDescription = alarmDescription;
    }

    public Integer getAlarmsStatus() {
        return alarmsStatus;
    }

    public void setAlarmsStatus(Integer alarmsStatus) {
        this.alarmsStatus = alarmsStatus;
    }

    public Integer getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public Date getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(Date alarmTime) {
        this.alarmTime = alarmTime;
    }

    public Integer getAlarmConfirmStatus() {
        return alarmConfirmStatus;
    }

    public void setAlarmConfirmStatus(Integer alarmConfirmStatus) {
        this.alarmConfirmStatus = alarmConfirmStatus;
    }
}

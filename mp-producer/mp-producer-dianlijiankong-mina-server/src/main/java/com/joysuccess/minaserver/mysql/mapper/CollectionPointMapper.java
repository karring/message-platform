package com.joysuccess.minaserver.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joysuccess.minaserver.mysql.model.SmCollectionPoint;


public interface CollectionPointMapper extends BaseMapper<SmCollectionPoint> {

}

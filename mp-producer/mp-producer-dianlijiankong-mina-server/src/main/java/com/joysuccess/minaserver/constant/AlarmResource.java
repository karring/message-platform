package com.joysuccess.minaserver.constant;

/**
 * 告警来源列表，未来可能会有很多告警来源，例如：动环，能源等告警，发送到kafka的时候，需要标记一下他们的来源
 * 如果发送方没有设置属性，那么当前常量就是维护一份自己的告警来源。
 */
public class AlarmResource {
    public static final String DYNAMIC_ALARM="dynamic-alarm";
}

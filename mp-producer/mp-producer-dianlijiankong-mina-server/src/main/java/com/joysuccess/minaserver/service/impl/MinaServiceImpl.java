package com.joysuccess.minaserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.joysuccess.common.utils.HelpUtils;
import com.joysuccess.minaserver.mysql.mapper.CollectionPointMapper;
import com.joysuccess.minaserver.mysql.model.SmCollectionPoint;
import com.joysuccess.minaserver.service.MinaService;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author : yaowenlong
 * @Description: mina相关接口
 * @date : 2020年06月13日 14:25
 */
@Service
@Log
public class MinaServiceImpl implements MinaService {
    private final static Logger LOGGER = LoggerFactory.getLogger(MinaServiceImpl.class);

    @Autowired
    private CollectionPointMapper collectionPointMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    private Map<String, Map<String,String>> equipPointsMap;

    public Map<String, Map<String,String>> getEquipPointsMap() {
        return equipPointsMap;
    }

    /**
     * 初始化Redis数据，数据来源：MySQL中的CollectionPoint表
     * @return
     */
    @Override
    public boolean initRedisFromMysql() {
        //1. select from mysql
        equipPointsMap = selectSnmpEquipPointsFromMysql();
        if(HelpUtils.isEmpty(equipPointsMap)) {
            LOGGER.error("初始化Redis数据的时候，此时数据库中的设备为空====，初始化失败");
            return false;
        }
        return true;
    }
    /**
     * 从sm表中查询数据，将SmCollectionPoint测点数据先放入JVM缓存Map<String,Set<String>中
     * 1.遍历存放于Redis中
     * 2.kafka生产者，生产数据的时候，作为数据源到，动环的snmp查询详细信息
     * @return
     */
    private Map<String,Map<String,String>> selectSnmpEquipPointsFromMysql() {
        //1.query from  mysql
        QueryWrapper<SmCollectionPoint> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SmCollectionPoint::getPointOrigin, "DLJK");
        List<SmCollectionPoint> colPointList = collectionPointMapper.selectList(queryWrapper);
        log.info("测点数量：" + colPointList.size());

        //备注一下，尽量的能够知道HashMap中的数据量，这样可以提交效率
        Map<String,Map<String,String>> equipOids = new HashMap<>();
        //2.loop result
        if(HelpUtils.isNotEmpty(colPointList)) {
            //循环遍历采集点表
            for (SmCollectionPoint smCollectionPoint : colPointList){
                //Assets Id当做键值
                String assetId = smCollectionPoint.getAssertId();
                if(HelpUtils.isEmpty(assetId)){
                    continue;
                }
                //1.存入Map中
                Map<String,String> oidList = equipOids.get(assetId);
                //如果Map中没有数据，就往这里面put数据一个新的HashMap
                if(HelpUtils.isEmpty(oidList)){
                    Map<String,String> newMap =  new HashMap<>();
                    newMap.put(smCollectionPoint.getPointCode(),"");
                    equipOids.put(assetId,newMap);
                }else{
                    //否则的话，将point_code存入，使用Set去重，point-code是唯一的
                    equipOids.get(assetId).put(smCollectionPoint.getPointCode(),"");
                }

                //2.初始化到Redis中
                redisTemplate.opsForHash().put(assetId,smCollectionPoint.getPointCode(),"");

            }
        }
        return equipOids;
    }
}

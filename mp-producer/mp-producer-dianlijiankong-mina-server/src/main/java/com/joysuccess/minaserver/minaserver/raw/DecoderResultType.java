package com.joysuccess.minaserver.minaserver.raw;

/**
 * 解码器统一返回数据类型
 */
public class DecoderResultType {

    //指令
    private String ins;
    //指令类型
    private String insType;
    //包体内容
    private String body;

    public String getIns() {
        return ins;
    }

    public void setIns(String ins) {
        this.ins = ins;
    }

    public String getInsType() {
        return insType;
    }

    public void setInsType(String insType) {
        this.insType = insType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "DecoderResultType{" +
                "ins='" + ins + '\'' +
                ", insType='" + insType + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}

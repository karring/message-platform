package com.joysuccess.minaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.stereotype.Controller;
/**
 * 生产者
 *
 * @author zhangqing
 * @date 2019年05月31日
 */
@SpringBootApplication
@EnableRedisRepositories
@Controller
public class MinaServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(MinaServerApplication.class,args);
    }
}

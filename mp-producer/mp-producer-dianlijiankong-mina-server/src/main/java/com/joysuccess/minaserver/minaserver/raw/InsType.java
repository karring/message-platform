package com.joysuccess.minaserver.minaserver.raw;

/**
 * 指令类型枚举
 */
public enum InsType {

    POINT_CONFIG("采集点配置","1")
    ,REPORT_SERVER_CONFIG("上报服务器配置","10")
    ,REAL_TIME_POINT_VALUE("实时采集值查询请求","11")
    ,HISTORY_POINT_VALUE("历史采集值查询请求","100")
    ,POINT_INFO("采集点信息查询","101")
    ,HISTORY_ALARM("告警历史查询","110")
    ;
    private String means;
    private String value;

    InsType(String means, String value) {
        this.means = means;
        this.value = value;
    }

    public static String getMeans(String value) {
        for (InsType insType : InsType.values()) {
            if (insType.getValue().equals(value)) {
                return insType.means;
            }
        }
        return null;
    }

    public String getMeans() {
        return means;
    }

    public void setMeans(String means) {
        this.means = means;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

package com.joysuccess.minaserver.utils;

import java.util.Random;
import java.util.UUID;

/**
 * @author zhangqing
 * @date 2019年04月30日
 */
public class ToolsUtil {

    public synchronized static String getNextCode() {
        return UUID.randomUUID().toString();
    }


    public synchronized static Float getFee() {
        Random rand = new Random();
        float fee = rand.nextFloat();

        return fee;
    }
}

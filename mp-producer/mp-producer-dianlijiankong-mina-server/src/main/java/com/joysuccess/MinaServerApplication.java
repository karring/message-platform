package com.joysuccess;

import com.joysuccess.minaserver.minaserver.MinaServer;
import com.joysuccess.minaserver.service.MinaService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 生产者
 *
 * @author zhangqing
 * @date 2019年05月31日
 */
@SpringBootApplication
@MapperScan("com.joysuccess.minaserver.mysql.mapper")
public class MinaServerApplication implements ApplicationRunner {

    @Value("${minaserver.port}")
    private Integer port;

    @Autowired
    private MinaService minaService;

    public static void main(String[] args) {
        SpringApplication.run(MinaServerApplication.class,args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        minaService.initRedisFromMysql();
        new MinaServer().run(port);
    }
}

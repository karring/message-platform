package com.joysuccess.snmp.sources.mysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joysuccess.snmp.sources.mysql.model.SmCollectionPoint;


public interface CollectionPointMapper extends BaseMapper<SmCollectionPoint> {

}

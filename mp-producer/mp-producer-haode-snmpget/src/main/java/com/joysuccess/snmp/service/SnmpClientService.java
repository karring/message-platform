package com.joysuccess.snmp.service;

import com.joysuccess.common.utils.HelpUtils;
import com.joysuccess.common.utils.ResponseJson;
import com.joysuccess.snmp.sources.mysql.mapper.CollectionPointMapper;
import com.joysuccess.snmp.sources.mysql.model.SmCollectionPoint;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SNMP接口层,包括数据库、Redis、Snmp本身等处理逻辑
 * @author zhangqing
 * @date 2019-12-12
 */
@Service
@Log
public class SnmpClientService {
    private final static Logger LOGGER = LoggerFactory.getLogger(SnmpClientService.class);

    @Autowired
    private CollectionPointMapper collectionPointMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    private Map<String,Map<String,String>> equipPointsMap;
    public Map<String, Map<String,String>> getEquipPointsMap() {
        return equipPointsMap;
    }

    /**
     * 初始化Redis数据，数据来源：MySQL中的CollectionPoint表
     * @return
     */
    public boolean initRedisFromMysql() {
        //1. select from mysql
        equipPointsMap = selectSnmpEquipPointsFromMysql();
        if(HelpUtils.isEmpty(equipPointsMap)) {
            LOGGER.error("初始化Redis数据的时候，此时数据库中的设备为空====，初始化失败");
            return false;
        }
        return true;
    }
    /**
     * 从sm表中查询数据，将SmCollectionPoint测点数据先放入JVM缓存Map<String,Set<String>中
     * 1.遍历存放于Redis中
     * 2.kafka生产者，生产数据的时候，作为数据源到，动环的snmp查询详细信息
     * @return
     */
    private Map<String,Map<String,String>> selectSnmpEquipPointsFromMysql() {
        //1.query from  mysql
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("point_origin","DHJK");
        List<SmCollectionPoint> colPointList = collectionPointMapper.selectByMap(paramMap);
//      List<SmCollectionPoint> colPointList = collectionPointMapper.selectList(null);
        //备注一下，尽量的能够知道HashMap中的数据量，这样可以提交效率
        Map<String,Map<String,String>> equipOids = new HashMap<>();
        //2.loop result
        if(HelpUtils.isNotEmpty(colPointList)) {
            //循环遍历采集点表
            for (SmCollectionPoint smCollectionPoint : colPointList){
                //Assets Id当做键值
                String assetId = smCollectionPoint.getAssertId();
                if(HelpUtils.isEmpty(assetId)){
                    continue;
                }
                //1.存入Map中
                Map<String,String> oidList = equipOids.get(assetId);
                //如果Map中没有数据，就往这里面put数据一个新的HashMap
                if(HelpUtils.isEmpty(oidList)){
                    Map<String,String> newMap =  new HashMap<>();
                    newMap.put(smCollectionPoint.getPointCode(),"");
                    equipOids.put(assetId,newMap);
                }else{
                    //否则的话，将point_code存入，使用Set去重，point-code是唯一的
                    equipOids.get(assetId).put(smCollectionPoint.getPointCode(),"");
                }

                //2.初始化到Redis中
                redisTemplate.opsForHash().put(assetId,smCollectionPoint.getPointCode(),"");

            }
        }
        return equipOids;
    }


//    /**
//     * 发送动环数据到kafka：先查询MySQL测点表数据并进行遍历，将遍历的oid当做SNMP链接的条件查询数据
//     * @param ipAddr
//     * @param port
//     * @return ResponseJson
//     */
//    public ResponseJson sendSnmpMsgToKafkaFromMySQL(String ipAddr, int port) {
//        //1.query from  mysql
//        List<SmCollectionPoint> colPointList = collectionPointMapper.selectList(null);
//        //2.loop result
//        if(HelpUtils.isEmpty(colPointList)) {return new ResponseJson(SnmpManager.SNMP_STATUS_MYSQL_404,SnmpManager.SNMP_STATUS_MYSQL_404_MSG);}
//        List<String> oidList = new ArrayList<>();
//        for (int i = 0; i < colPointList.size(); i++) {
//            //是否批量发送
//            if(sendBatch(oidList.size())) {
//                //batch get results to send kafka
//                batchGetSnmpSendToKafka(ipAddr,port,oidList);
//                //发送数据失败了，因为oidList没有清空
//               if(oidList.size() > 0) {
//                    //进行三次尝试到kafka中=======================to do =================
//               }
//
//            }
//            oidList.add(colPointList.get(i).getPointCode());
//        }
//        /**
//         * 剩下的条数进行处理
//         */
//        if(HelpUtils.isNotEmpty(oidList)) {
//            batchGetSnmpSendToKafka(ipAddr,port,oidList);
//        }
//        return new ResponseJson(SnmpManager.SNMP_STATUS_YES,SnmpManager.SNMP_STATUS_YES_MSG);
//    }
//
//    private void batchGetSnmpSendToKafka(String ipAddr,int port,List<String> oidList)  {
//        try {
//            //批量获取数据
//            Map<String,String> batchList = SnmpManager.snmpAsynGetList(ipAddr,port,oidList);
//            for (Map.Entry<String, String> entry : batchList.entrySet()) {
//                if ("Null".equals(entry.getValue())) {
//                    break;
//                }
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("collection_code",entry.getKey());
//                jsonObject.put("collection_value",entry.getValue());
//                kafkaTemplate.send(TOPIC_NAME_DYNAMIC,jsonObject.toJSONString());
//                jsonObject = null;
//            }
//            //如果发送数据到kafka成功，重置oidList集合，给GC回收
//            oidList = new ArrayList<>();
//        } catch (IOException e) {
//            log.info("snmp exception: batchGetSnmpSendToKafka---IOException " + e.getMessage());
//        } catch (InterruptedException e) {
//            log.info("snmp exception: batchGetSnmpSendToKafka---InterruptedException" + e.getMessage());
//        }
//    }
//
//    private boolean sendBatch(int listCount) {
//        return listCount == snmpBatchSize;
//    }

//
//    /**
//     * 发送动环数据到kafka：先查询Redis测点表数据并进行遍历，将遍历的oid当做SNMP链接的条件查询数据
//     * @param ip
//     * @param port
//     */
//    public ResponseJson sendSnmpMsgToKafkaFromRedis(String ipAddr, int port) {
//        //1.query from  redis
//        //2.loop result
//        //3.result's record to kafka
//        return null;
//    }

    public ResponseJson checkSnmpConnection(String ipAddr, int port) {

        return null;
    }

    public ResponseJson checkSnmpOid(String ipAddr, int port, String oid) {
        return null;
    }


}

//package com.joysuccess.snmp.producer;
//
//import com.alibaba.fastjson.JSONObject;
//import com.joysucces.common.utils.HelpUtils;
//import com.joysuccess.snmp.manager.SnmpManager;
//import com.joysuccess.snmp.service.SnmpClientService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.stereotype.Component;
//
//import java.io.IOException;
//import java.util.Map;
//import java.util.Set;
//
///**
// * @author : zhangqing
// * @Description: 遍历设备和oid关联的List
// * 数据结构为
// * {equipt:{"oid":"","oid":"".......}},{equipt:{"oid":"","oid":"".......}}
// * @date : 2020年05月29日 14:17
// */
//@Component
//public class SnmpDataProducer  {
//    private final static Logger LOGGER = LoggerFactory.getLogger(SnmpDataProducer.class);
//
//    @Autowired
//    private SnmpClientService snmpClientService;
//    @Autowired
//    private KafkaTemplate kafkaTemplate;
//    private final static String DYNAMIC_DATAS_TOPIC="haode-dynamic-datas";
//    @Value("${snmp.datas.ip}")
//    private String snmpId;
//    @Value("${snmp.datas.port}")
//    private Integer snmpPort;
//
//    /**
//     * 循环遍历JVM中的equipPointsList数据
//     */
//    public void sendSnmpDatasToKafka(){
//        //1.检查snmp连接是否正常
////        SnmpManager.createDefault(snmpId,snmpPort);
//        //2.开始循环处理
//            Map<String,Map<String,String>> equipPointsList = snmpClientService.getEquipPointsMap();
//            if(HelpUtils.isNotEmpty(equipPointsList)){
//                //keySet是设备编号，值是设备对应的采集点信息
//                Set<String> assetListPoints = equipPointsList.keySet();
//                for (String assetId: assetListPoints) {
//                    //获取采集点数据集合
//                    Map<String,String> oidLists = equipPointsList.get(assetId);
//                    Set<String> oidIds =  oidLists.keySet();
//                    try {
//                        //获取采集点的值，获取的结果是oid:varies信息
//                        Map<String,String> resultMap = SnmpManager.snmpGetListBySet(snmpId,snmpPort,oidIds);
//                        //将map转换为JSONString便于数据发送到Kafka中
//                        String oidsList = JSONObject.toJSONString(resultMap);
//                        JSONObject equipOids = new JSONObject();
//                        //发送到kafka数据的格式是：设备：{oid:varies,oid:varies}，也就是当前设备下的采集点信息
//                        equipOids.put("assetId",assetId);
//                        equipOids.put("oidList",oidsList);
//                        kafkaTemplate.send(DYNAMIC_DATAS_TOPIC,equipOids.toJSONString());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }else {
//                LOGGER.warn("SNMP 数据发送到 Kafka失败，因为还没有进行Redis的数据的初始化，继续循环，等待数据被初始化");
//            }
//        //connectors/dynamic-kafka-es-connecotr/task/dynamic-kafka-es-connecotr-0/restart
//    }
//}

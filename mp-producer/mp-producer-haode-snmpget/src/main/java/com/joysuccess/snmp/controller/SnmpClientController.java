package com.joysuccess.snmp.controller;

import com.joysuccess.common.utils.ResponseJson;
import com.joysuccess.snmp.manager.SnmpManager;
import com.joysuccess.snmp.service.SnmpClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * snmp协议控制器
 * @author zhangqing
 * @date 2019-12-12
 */
@RestController
@RequestMapping("/snmp/dynamic")
@Api(value = "SnmpClientController" ,description = "动环实时数据控制器")
public class SnmpClientController {

    private final static Integer INIT_REDIS_STATUS = 200;
    private final static String INIT_REDIS_FAIL = "初始化Redis失败";
    private final static String INIT_REDIS_SUCCESS = "初始化Redis成功";

    @Autowired
    private SnmpClientService snmpService;

    /**
     * 全量初始化Redis数据
     * 从MySQL中获取测点数据，并将数据初始化到Redis中
     * @return
     */
    @ApiOperation(value = "手动全量初始化Redis一次,测点数据源来自从MySQL查询数据")
    @PostMapping("/init-redis-from-mysql-once-all")
    public ResponseJson initSnmpDatasToRedisFromMySQL() {
       boolean isSuccess = snmpService.initRedisFromMysql();
       return new ResponseJson(INIT_REDIS_STATUS,isSuccess==true ? INIT_REDIS_SUCCESS : INIT_REDIS_FAIL);
    }

    /**
     * 根据ip和端口号，检查snmp连接是联通
     * @param ipAddr
     * @param port
     * @return
     */
//    @PostMapping("/check")
//    public ResponseJson checkSnmpConnection(String ipAddr, int port) {
//        return snmpService.checkSnmpConnection(ipAddr, port);
//    }

    /**
     * 根据ip、端口号和oid检查，oid是否存在
     * @param oid
     * @param ipAddr
     * @param port
     * @return
     */
//    @PostMapping("/check/{oid}")
//    public ResponseJson checkSnmpConnection(@PathVariable("oid") String oid, String ipAddr, int port) {
//        return snmpService.checkSnmpOid(ipAddr, port,oid);
//    }

    /**
     * 从MySQL中获取测点数据进行遍历，遍历的过程进行和厂商的SNMP链接并且获取数据，进入Kafka
     * @return
     */
    @PostMapping("/mysql")
    public ResponseJson useMySQLSource(String ipAddr,String port) {
        //1.check connection for snmp and mysql's collection_point

        //2.process current logic,query mysql for datas,loop the datas for snmp query,then send to kafka
        return new ResponseJson(SnmpManager.SNMP_STATUS_YES,SnmpManager.SNMP_STATUS_YES_MSG);
    }

    /**
     * 从Redis中获取测点数据进行遍历，遍历的过程进行和厂商的SNMP链接并且获取数据，进入Kafka
     * @return
     */
    @PostMapping("/redis")
    public ResponseJson useRedisSource(String ipAddr,String port) {

        return new ResponseJson(SnmpManager.SNMP_STATUS_YES,SnmpManager.SNMP_STATUS_YES_MSG);
    }

}

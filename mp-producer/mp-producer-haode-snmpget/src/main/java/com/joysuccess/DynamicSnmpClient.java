package com.joysuccess;

import com.joysuccess.queue.core.QueueContext;
import com.joysuccess.queue.core.QueueTypeHandler;
import com.joysuccess.snmp.producer.DynamicMonitorSnmpGetProtocol;
import com.joysuccess.snmp.service.SnmpClientService;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhangqing
 */
@SpringBootApplication
@MapperScan("com.joysuccess.snmp.sources.mysql.mapper")
public class DynamicSnmpClient implements CommandLineRunner {
    private final static Logger LOGGER = LoggerFactory.getLogger(DynamicSnmpClient.class);

    @Autowired
    private QueueContext queueContext;

    @Autowired
    private QueueTypeHandler queueTypeHandler;

    @Autowired
    private SnmpClientService snmpClientService;

    @Value("${queue.type}")
    private String queueType;

    public static void main(String[] args) {
        SpringApplication.run(DynamicSnmpClient.class,args);
    }

    @Override
    public void run(String... args) throws Exception {
        /**
         * 创建一个单独的线程做循环获取SNMP数据，并发送到Kafka中
         */
        LOGGER.info("SnmpDatasProducerThread线程开启--->准备循环获取SNMP数据，并发送到Kafka中");
        new Thread(()->{
            /** 1.在项目启动的时候，从MySQL中初始化Redis的数据
             *  在获取的同事也同事保存于JVM中的缓存中，作为sendSnmpDatasToKafka
             *  数据源
             */
            snmpClientService.initRedisFromMysql();
            DynamicMonitorSnmpGetProtocol dynamicMonitorSnmpGetProtocol = new DynamicMonitorSnmpGetProtocol();
            for(;;){
                //2.sendSnmpDatasToKafka
                dynamicMonitorSnmpGetProtocol.getDataToSend();
                try {
                    //30秒轮训一次
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"SnmpDatasProducerThread").start();
    }
}

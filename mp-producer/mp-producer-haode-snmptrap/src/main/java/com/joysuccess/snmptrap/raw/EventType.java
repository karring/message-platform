package com.joysuccess.snmptrap.raw;

public enum EventType {
    ALARMEVENT("告警事件", "01"), RESTOREEVENT("恢复事件", "02");
    private String means;
    private String value;

    EventType(String means, String value) {
        this.means = means;
        this.value = value;
    }

    public static String getMeans(String value) {
        for (EventType eventType : EventType.values()) {
            if (eventType.getValue().equals(value)) {
                return eventType.means;
            }
        }
        return null;
    }

    public String getMeans() {
        return means;
    }

    public void setMeans(String means) {
        this.means = means;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

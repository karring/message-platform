package com.joysuccess.snmptrap.raw;

public enum EventField {
    TEMPOID("1.3.6.1.6.3.1.1.4.1.0", "tempOid", "tempOid"),
    UNIQUEID("1.3.6.1.4.1.93450.3.93451.1.2", "uniqueId", "自增id"),
    EVENTLEVEL("1.3.6.1.4.1.93450.3.93451.1.3", "eventLevel", "事件等级"),
    STARTTIME("1.3.6.1.4.1.93450.3.93451.1.4", "startTime", "开始时间"),
    ENDTIME("1.3.6.1.4.1.93450.3.93451.1.5", "endTime", "结束时间"),
    ENDEVENTSTATE("1.3.6.1.4.1.93450.3.93451.1.6", "endEventState", "事件结束状态"),
    IPADDRESS("1.3.6.1.4.1.93450.3.93451.1.7", "ipAddress", "IP地址"),
    EVENTMEANING("1.3.6.1.4.1.93450.3.93451.1.8", "eventMeaning", "事件内容"),
    EVENTONLYID("1.3.6.1.4.1.93450.3.93451.1.9", "eventOnlyID", "事件唯一ID"),
    ROOMID("1.3.6.1.4.1.93450.3.93451.1.19", "roomID", ""),
    EQUIPID("1.3.6.1.4.1.93450.3.93451.1.11", "equipID", ""),
    EVENTID("1.3.6.1.4.1.93450.3.93451.1.12", "eventID", "事件ID"),
    CONDITIONID("1.3.6.1.4.1.93450.3.93451.1.13", "conditionID", ""),
    STARTCOMPAREVALUE("1.3.6.1.4.1.93450.3.93451.1.14", "startCompareValue", "触发阈值"),
    EVENTVALUE("1.3.6.1.4.1.93450.3.93451.1.15", "eventValue", "触发值"),
    ENDCOMPAREVALUE("1.3.6.1.4.1.93450.3.93451.1.16", "endCompareValue", "恢复阈值"),
    ENDVALUE("1.3.6.1.4.1.93450.3.93451.1.17", "endValue", "恢复值"),
    ALARMDETAILEDDESCRIPTION("1.3.6.1.4.1.93450.3.93451.1.18", "alarmdetaileddescription", "告警内容");

    private String oid;
    private String means;
    private String fieldName;


    EventField(String oid, String fieldName, String means) {
        this.oid = oid;
        this.fieldName = fieldName;
        this.means = means;
    }

    public static String getFieldName(String oid) {
        for (EventField eventField : EventField.values()) {
            if (eventField.getOid().equals(oid)) {
                return eventField.getFieldName();
            }
        }
        return null;
    }

    public static Object getMeans(String oid) {
        for (EventField eventField : EventField.values()) {
            if (eventField.getMeans().equals(oid)) {
                return eventField.getMeans();
            }
        }
        return null;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getMeans() {
        return means;
    }

    public void setMeans(String means) {
        this.means = means;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}

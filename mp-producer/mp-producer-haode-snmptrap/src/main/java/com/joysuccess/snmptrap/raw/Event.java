package com.joysuccess.snmptrap.raw;

import java.util.Date;

public class Event {
    private String tempOid;
    private String uniqueId;
    private Integer eventLevel;//告警等级
    private Date startTime;//告警开始时间
    private Date endTime;//告警结束时间
    private String endEventState;
    private String ipAddress;//ip地址
    private String eventMeaning;
    private String eventOnlyID;
    private String roomID;//机房ID
    private String equipID;//设备ID
    private String eventID;
    private String conditionID;
    private String startCompareValue;
    private String eventValue;//触发值
    private String endCompareValue;
    private String endValue;
    private String alarmdetaileddescription;//告警内容

    public String getTempOid() {
        return tempOid;
    }

    public void setTempOid(String tempOid) {
        this.tempOid = tempOid;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Integer getEventLevel() {
        return eventLevel;
    }

    public void setEventLevel(Integer eventLevel) {
        this.eventLevel = eventLevel;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getEndEventState() {
        return endEventState;
    }

    public void setEndEventState(String endEventState) {
        this.endEventState = endEventState;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getEventMeaning() {
        return eventMeaning;
    }

    public void setEventMeaning(String eventMeaning) {
        this.eventMeaning = eventMeaning;
    }

    public String getEventOnlyID() {
        return eventOnlyID;
    }

    public void setEventOnlyID(String eventOnlyID) {
        this.eventOnlyID = eventOnlyID;
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public String getEquipID() {
        return equipID;
    }

    public void setEquipID(String equipID) {
        this.equipID = equipID;
    }

    public String getEventID() {
        return eventID;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public String getConditionID() {
        return conditionID;
    }

    public void setConditionID(String conditionID) {
        this.conditionID = conditionID;
    }

    public String getStartCompareValue() {
        return startCompareValue;
    }

    public void setStartCompareValue(String startCompareValue) {
        this.startCompareValue = startCompareValue;
    }

    public String getEventValue() {
        return eventValue;
    }

    public void setEventValue(String eventValue) {
        this.eventValue = eventValue;
    }

    public String getEndCompareValue() {
        return endCompareValue;
    }

    public void setEndCompareValue(String endCompareValue) {
        this.endCompareValue = endCompareValue;
    }

    public String getEndValue() {
        return endValue;
    }

    public void setEndValue(String endValue) {
        this.endValue = endValue;
    }

    public String getAlarmdetaileddescription() {
        return alarmdetaileddescription;
    }

    public void setAlarmdetaileddescription(String alarmdetaileddescription) {
        this.alarmdetaileddescription = alarmdetaileddescription;
    }
}

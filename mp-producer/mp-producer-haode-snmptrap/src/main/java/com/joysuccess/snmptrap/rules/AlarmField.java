package com.joysuccess.snmptrap.rules;

public enum AlarmField {
    ALARMINDEX("1.3.6.1.4.1.74226.3.3012.3.1.2.1.1.1", "alarmIndex", "告警索引"),
    EQUIPID("1.3.6.1.4.1.74226.3.3012.3.1.2.1.1.2", "equipId", "设备ID"),
    PIOINTID("1.3.6.1.4.1.74226.3.3012.3.1.2.1.3.1", "pointId", "点位ID"),
    ALARMSTATUS("1.3.6.1.4.1.74226.3.3012.3.1.2.1.4.1", "alarmsStatus", "告警状态"),
    ALARMLEVEL("1.3.6.1.4.1.74226.3.3012.3.1.2.1.5.1", "alarmLevel", "告警等级"),
    ALARMTIME("1.3.6.1.4.1.74226.3.3012.3.1.2.1.6.1", "alarmTime", "告警产生时间"),
    CURRENTVALUE("1.3.6.1.4.1.74226.3.3012.3.1.2.1.7.1", "currentValue", "当前值"),
    ALARMDESCRIPTION("1.3.6.1.4.1.74226.3.3012.3.1.2.1.8.1", "alarmDescription", "告警描述"),
    ALARMCONFIRMSTATUS("1.3.6.1.4.1.74226.3.3012.3.1.2.1.9.1", "alarmConfirmStatus", "告警确认状态");
    private String oid;
    private String description;
    private String fieldName;

    AlarmField(String oid, String fieldName, String description) {
        this.oid = oid;
        this.fieldName = fieldName;
        this.description = description;
    }

    public static String getFieldNameByContains(String oid) {
        for (AlarmField eventField : AlarmField.values()) {
            if (eventField.getOid().contains(oid)) {
                return eventField.getFieldName();
            }
        }
        return null;
    }

    public static String getFieldName(String oid) {
        for (AlarmField eventField : AlarmField.values()) {
            if (eventField.getOid().equals(oid)) {
                return eventField.getFieldName();
            }
        }
        return null;
    }

    public static Object getMeans(String oid) {
        for (AlarmField eventField : AlarmField.values()) {
            if (eventField.getDescription().equals(oid)) {
                return eventField.getDescription();
            }
        }
        return null;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
}

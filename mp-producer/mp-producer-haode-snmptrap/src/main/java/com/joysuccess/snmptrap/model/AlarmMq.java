package com.joysuccess.snmptrap.model;

import java.util.Date;

/**
 * 动环告警存MQ队列统一格式
 *
 * @author shenchao
 * @date 2017/10/14
 */
public class AlarmMq{

    private String alarmId; //告警ID
    private Boolean isAlarmRecover;  //是否为恢复告警
    private String collCode;    //告警(代理、设备、测点)编码
    private String ip;  //代理ip(只有代理告警该项才有值)
    private Integer alarmLevel; //告警级别
    private String alarmState; //事件状态 （超上限 超下限 临近上限 临近下限数据异常 测定状态变化 告警回复 代理和设备联机 代理和设备脱机 文本报警）
    private String alarmTitle;  //告警标题
    private String alarmContent;    //告警内容
    private Date alarmDate;   //告警时间

    public String getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(String alarmId) {
        this.alarmId = alarmId;
    }

    public Boolean getAlarmRecover() {
        return isAlarmRecover;
    }

    public void setAlarmRecover(Boolean alarmRecover) {
        isAlarmRecover = alarmRecover;
    }

    public String getCollCode() {
        return collCode;
    }

    public void setCollCode(String collCode) {
        this.collCode = collCode;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getAlarmLevel() {
        return alarmLevel;
    }

    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    public String getAlarmState() {
        return alarmState;
    }

    public void setAlarmState(String alarmState) {
        this.alarmState = alarmState;
    }

    public String getAlarmTitle() {
        return alarmTitle;
    }

    public void setAlarmTitle(String alarmTitle) {
        this.alarmTitle = alarmTitle;
    }

    public String getAlarmContent() {
        return alarmContent;
    }

    public void setAlarmContent(String alarmContent) {
        this.alarmContent = alarmContent;
    }

    public Date getAlarmDate() {
        return alarmDate;
    }

    public void setAlarmDate(Date alarmDate) {
        this.alarmDate = alarmDate;
    }
}

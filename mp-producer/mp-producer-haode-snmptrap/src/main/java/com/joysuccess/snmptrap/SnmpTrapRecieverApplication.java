package com.joysuccess.snmptrap;

import com.joysuccess.snmptrap.snmptrap.SnmpTrapMultiThreadReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * @author : zhangqing
 * @Description: TODO
 * @date : 2020年05月28日 10:51
 */
@ComponentScan("com.joysuccess")
@SpringBootApplication
public class SnmpTrapRecieverApplication implements CommandLineRunner {

    private final static Logger LOGGER = LoggerFactory.getLogger(SnmpTrapRecieverApplication.class);


    @Value("${snmp.trap.ip}")
    private String snmpTrapIp;
    @Value("${snmp.trap.port}")
    private String snmpTrapPort;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    public static void main(String[] args) {
        SpringApplication.run(SnmpTrapRecieverApplication.class,args);
    }

    /**
     * springboot启动之后运行，这里直接启动SnmpTrap
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        SnmpTrapMultiThreadReceiver snmpTrapMultiThreadReceiver = new SnmpTrapMultiThreadReceiver(kafkaTemplate,snmpTrapIp,snmpTrapPort);
        snmpTrapMultiThreadReceiver.trapReceiverRun();
    }
}

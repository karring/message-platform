package com.joysuccess.snmptrap.raw;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;

/**
 * author:xuzhuang
 * Time:15:18
 */
public class SnmpSendTrap {
    private Snmp snmp = null;
    private Address targetAddress = null;
    private String snmpIp;
    private String snmpPort;

    public SnmpSendTrap(String snmpIp, String snmpPort) {
        this.snmpIp = snmpIp;
        this.snmpPort = snmpPort;
    }

    public void initComm() throws IOException {
        // 设置Agent方的IP和端口
        targetAddress = GenericAddress.parse("udp:" + snmpIp + "/" + snmpPort);
        TransportMapping transport = new DefaultUdpTransportMapping();
        snmp = new Snmp(transport);
        transport.listen();
    }

    public ResponseEvent sendPDU(PDU pdu) throws IOException {
        // 设置 目标
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString("public"));
        target.setAddress(targetAddress);
        // 通信不成功时的重试次数 N+1次
        target.setRetries(2);
        // 超时时间
        target.setTimeout(2 * 1000);
        // SNMP 版本
        target.setVersion(SnmpConstants.version2c);
        // 向Agent发送PDU，并返回Response
        return snmp.send(pdu, target);
    }

    public Vector<? extends VariableBinding> getPDU(PDU pdu) throws IOException {
        Vector<? extends VariableBinding> recVBs = null;
        pdu.setType(PDU.GET);
        ResponseEvent revent = sendPDU(pdu);
        if (revent != null && revent.getResponse() != null) {
            recVBs = revent.getResponse().getVariableBindings();
        }
        return recVBs;
    }

    public String getCode(int port, int u) throws IOException {
        initComm();
        String aotuOid = "1.3.6.1.4.1.42714.7.1.7.1.1.6." + port + "." + u;//位置信息
        PDU pduGet = new PDU();
        pduGet.add(new VariableBinding(new OID(aotuOid)));
        Vector<? extends VariableBinding> recVBs = getPDU(pduGet);
        String code="";
        for (int i = 0; i < recVBs.size(); i++) {
            String oid = recVBs.get(i).getOid().toString();
            code = recVBs.get(i).getVariable().toString();
        }
        return code;
    }
    /**
     * author   xuzhuang
     * dec    设置为自动模式
     */
    public boolean setAuto(int port, int u) throws IOException {
        initComm();
        String aotuOid = "1.3.6.1.4.1.42714.7.1.7.1.1.2." + port + "." + u;//shoudong
        PDU pduGet = new PDU();
        PDU pduSet = new PDU();
        pduGet.add(new VariableBinding(new OID(aotuOid)));
        Vector<? extends VariableBinding> recVBs = getPDU(pduGet);
        for (int i = 0; i < recVBs.size(); i++) {
            String oid = recVBs.get(i).getOid().toString();
            String value = recVBs.get(i).getVariable().toString();
            if (oid.equals(aotuOid) && !value.equals("0")) {
                pduSet.add(new VariableBinding(new OID(aotuOid), new Integer32(0)));
            }
        }
        pduSet.setType(PDU.SET);
        ResponseEvent re = sendPDU(pduSet);
        if (re != null && re.getResponse() != null && re.getResponse().getErrorStatus() == 0) {
            return true;
        }
        return false;
    }

    /**
     * author   xuzhuang
     * dec    设置为红灯
     */
    public boolean setRedCoder(int port, int u) throws IOException {
        initComm();
        String aotuOid = "1.3.6.1.4.1.42714.7.1.7.1.1.2." + port + "." + u;//shoudong
        String colorOid = "1.3.6.1.4.1.42714.7.1.7.1.1.5." + port + "." + u;//hongse
        PDU pduGet = new PDU();
        PDU pduSet = new PDU();
        pduGet.add(new VariableBinding(new OID(aotuOid)));
        pduGet.add(new VariableBinding(new OID(colorOid)));
        Vector<? extends VariableBinding> recVBs = getPDU(pduGet);
        for (int i = 0; i < recVBs.size(); i++) {
            String oid = recVBs.get(i).getOid().toString();
            String value = recVBs.get(i).getVariable().toString();
            if (oid.equals(aotuOid) && !value.equals("3")) {
                pduSet.add(new VariableBinding(new OID(aotuOid), new Integer32(3)));
            } else if (oid.equals(colorOid) && !value.equals("R")) {
                pduSet.add(new VariableBinding(new OID(colorOid), new OctetString("R")));
            }
        }
        pduSet.setType(PDU.SET);
        ResponseEvent re = sendPDU(pduSet);
        if (re != null && re.getResponse() != null && re.getResponse().getErrorStatus() == 0) {
            return true;
        }
        return false;
    }

    /**
     * author   xuzhuang
     * dec    设置为绿色快闪
     */
    public boolean setGreenQuick(int port, int u) throws IOException {
        initComm();
        String aotuOid = "1.3.6.1.4.1.42714.7.1.7.1.1.2." + port + "." + u;//shoudong
        String quick =   "1.3.6.1.4.1.42714.7.1.7.1.1.3." + port + "." + u;//kuaishan
        String colorOid ="1.3.6.1.4.1.42714.7.1.7.1.1.5." + port + "." + u;//hongse
        PDU pduGet = new PDU();
        PDU pduSet = new PDU();
        pduGet.add(new VariableBinding(new OID(aotuOid)));
        pduGet.add(new VariableBinding(new OID(quick)));
        pduGet.add(new VariableBinding(new OID(colorOid)));
        Vector<? extends VariableBinding> recVBs = getPDU(pduGet);
        for (int i = 0; i < recVBs.size(); i++) {
            String oid = recVBs.get(i).getOid().toString();
            String value = recVBs.get(i).getVariable().toString();
            if (oid.equals(aotuOid) && !value.equals("3")) {
                pduSet.add(new VariableBinding(new OID(aotuOid), new Integer32(3)));
            } else if (oid.equals(quick) && !value.equals("3")) {
                pduSet.add(new VariableBinding(new OID(quick), new Integer32(3)));
            } else if (oid.equals(colorOid) && !value.equals("G")) {
                pduSet.add(new VariableBinding(new OID(colorOid), new OctetString("G")));
            }
        }
        pduSet.setType(PDU.SET);
        ResponseEvent re = sendPDU(pduSet);
        if (re != null && re.getResponse() != null && re.getResponse().getErrorStatus() == 0) {
            return true;
        }
        return false;
    }

    /**
     * 边蓝灯
     * author   xuzhuang
     * dec 边蓝灯
     */
    public boolean setBlueCoder(int port, int u) throws IOException {
        initComm();
        String aotuOid = "1.3.6.1.4.1.42714.7.1.7.1.1.2." + port + "." + u;//shoudong
        String colorOid = "1.3.6.1.4.1.42714.7.1.7.1.1.5." + port + "." + u;//lanse
        PDU pduGet = new PDU();
        PDU pduSet = new PDU();
        pduGet.add(new VariableBinding(new OID(aotuOid)));
        pduGet.add(new VariableBinding(new OID(colorOid)));
        Vector<? extends VariableBinding> recVBs = getPDU(pduGet);
        for (int i = 0; i < recVBs.size(); i++) {
            String oid = recVBs.get(i).getOid().toString();
            String value = recVBs.get(i).getVariable().toString();
            if (oid.equals(aotuOid) && !value.equals("3")) {
                pduSet.add(new VariableBinding(new OID(aotuOid), new Integer32(3)));
            } else if (oid.equals(colorOid) && !value.equals("B")) {
                pduSet.add(new VariableBinding(new OID(colorOid), new OctetString("B")));
            }
        }
        pduSet.setType(PDU.SET);
        ResponseEvent re = sendPDU(pduSet);
        if (re != null && re.getResponse()!= null && re.getResponse().getErrorStatus() == 0) {
            return true;
        }
        return false;
    }
    
    @SuppressWarnings("unchecked")
    public void readResponse(ResponseEvent respEvnt) {
        // 解析Response
        System.out.println("------------>解析Response<-------------");
        if (respEvnt != null && respEvnt.getResponse() != null) {
            Vector<? extends VariableBinding> recVBs = respEvnt.getResponse()
                    .getVariableBindings();
            for (int i = 0; i < recVBs.size(); i++) {
                VariableBinding recVB = recVBs.elementAt(i);
                System.out.println(recVB.getOid() + " : "
                        + recVB.getVariable().toString());
            }
        }
    }

    public static String getIp2() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            return addr.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        try {
            SnmpSendTrap util = new SnmpSendTrap("localhost", "10002");
            util.initComm();

            util.setBlueCoder(0,12);
//            util.setRedCoder(0,12);
//            util.setRedCoder(12, 4);
//            util.setAuto(12, 5);
//            PDU pdu = new PDU();
//            pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.42714.7.1.7.1.1.5.0")));
//            pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.42714.7.1.7.1.1.2.0")));
//            pdu.setType(PDU.GET);
//            ResponseEvent re = util.sendPDU(pdu);
//            re.getResponse();
            /*PDU pdu = new PDU();
            pdu.add(new VariableBinding(new OID("1.3.6.1.4.1.42714.7.1.7.1.1.2.0.5")));
            util.getPDU(pdu);*/
            System.out.println(getIp2());
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}

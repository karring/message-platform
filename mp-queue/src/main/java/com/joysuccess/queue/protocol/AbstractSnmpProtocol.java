package com.joysuccess.queue.protocol;

import com.joysuccess.common.utils.SpringUtil;
import com.joysuccess.queue.core.OuterProtocol;
import com.joysuccess.queue.core.ProducerStrategy;
import com.joysuccess.queue.core.QueueTypeHandler;

/**
 * @author : zhangqing
 * @Description: snmp抽象类
 * @date : 2020年06月10日 20:35
 */
public abstract class AbstractSnmpProtocol implements OuterProtocol {
    /**
     * 消息队列类型
     */
    private String queueType = SpringUtil.getApplicationContext().getEnvironment().getProperty("queue.type");
    private QueueTypeHandler queueTypeHandler = SpringUtil.getBean(QueueTypeHandler.class);
    public ProducerStrategy producerStrategy = queueTypeHandler.getQueueType(queueType);

}

package com.joysuccess.queue.core;

/**
 * @author : zhangqing
 * @Description: 外部系统接口，将外部系统抽象成为外部协议
 * @date : 2020年06月10日 14:22
 */
public interface OuterProtocol {
    /**
     * 获取外部系统的数据并准备发送
     */
    public void getDataToSend();
}

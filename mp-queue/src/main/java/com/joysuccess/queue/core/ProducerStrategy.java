package com.joysuccess.queue.core;

/**
 * @author : zhangqing
 * @Description: 消息队列生产者策略，ActiveMQ，Kafka，RabbitMq等等
 * @date : 2020年06月08日 16:33
 */
public interface ProducerStrategy<T> {
    /**
     * 数据发送到队列
     * @param data
     * @param queue
     */
    public void sendRecordToQueue(String queue,String data);

    /**
     * 获取消息队列生产者的客户端
     * @return
     */
    public T getProducerClient();

}

package com.joysuccess.queue.core;

import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;

/**
 * @author : zhangqing
 * @Description: 通过反射，获取消息队列具体的实现类
 * @date : 2020年06月08日 20:55
 */
@Service
public class QueueTypeHandler {

    public ProducerStrategy getQueueType(String queueProducerClass) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(queueProducerClass);
            ProducerStrategy producerStrategy = (ProducerStrategy)clazz.getMethod("getInstance").invoke(null,null);
            return producerStrategy;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }
}

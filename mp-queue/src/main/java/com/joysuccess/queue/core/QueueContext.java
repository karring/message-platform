package com.joysuccess.queue.core;

import org.springframework.stereotype.Component;

/**
 * @author : zhangqing
 * @Description: 策略模式中的Context
 * @date : 2020年06月08日 17:40
 */
@Component
public class QueueContext {

    private ProducerStrategy producerStrategy;

    /**
     * 设置消息队列使用具体哪个策略
     * @param producerStrategy
     */
    public void setQueueStrategy(ProducerStrategy producerStrategy) {
        this.producerStrategy = producerStrategy;
    }

    /**
     * 执行指定消息队列具体发送数据的策略
     */
    public void sendDataToQueue(String queue,String data) {
        producerStrategy.sendRecordToQueue(queue,data);
    }
}

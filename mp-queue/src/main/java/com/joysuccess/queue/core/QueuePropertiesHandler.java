package com.joysuccess.queue.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author : zhangqing
 * @Description: 消息队列配置文件读取
 * @date : 2020年06月08日 21:48
 */
@PropertySource(value = "classpath:queue.yml",factory = CompositePropertySourceFactory.class)
@Component
public class QueuePropertiesHandler {

    /**
     *  -----------Kafka配置参数开始-----------
     */
    @Value("${queue.kafka.bootstrap-servers}")
    private String kafkaServers;
    @Value("${queue.kafka.acks}")
    private String acks;
    @Value("${queue.kafka.retries}")
    private String retries;
    @Value("${queue.kafka.batch-size}")
    private String batchSize;
    @Value("${queue.kafka.linger-ms}")
    private String lingerMs;
    @Value("${queue.kafka.buffer-memory}")
    private String bufferMemory;
    @Value("${queue.kafka.client.id}")
    private String clientId;
    @Value("${queue.kafka.key-serializer}")
    private String keySerializer;
    @Value("${queue.kafka.value-serializer}")
    private String valueSerializer;
    /**
     *  -----------Kafka配置参数结束-----------
     */


    /**
     *  -----------rabbitmq配置参数开始-----------
     */
    @Value("${queue.rabbitmq.username}")
    private String userName;
    @Value("${queue.rabbitmq.password}")
    private String password;
    @Value("${queue.rabbitmq.host}")
    private String host;
    @Value("${queue.rabbitmq.port}")
    private Integer port;
    /**
     *  -----------rabbitmq配置参数结束-----------
     */

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     *  -----------rabbitmq配置参数结束-----------
     */


    public String getAcks() {
        return acks;
    }

    public void setAcks(String acks) {
        this.acks = acks;
    }

    public String getRetries() {
        return retries;
    }

    public void setRetries(String retries) {
        this.retries = retries;
    }

    public String getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(String batchSize) {
        this.batchSize = batchSize;
    }

    public String getLingerMs() {
        return lingerMs;
    }

    public void setLingerMs(String lingerMs) {
        this.lingerMs = lingerMs;
    }

    public String getBufferMemory() {
        return bufferMemory;
    }

    public void setBufferMemory(String bufferMemory) {
        this.bufferMemory = bufferMemory;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getKeySerializer() {
        return keySerializer;
    }

    public void setKeySerializer(String keySerializer) {
        this.keySerializer = keySerializer;
    }

    public String getValueSerializer() {
        return valueSerializer;
    }

    public void setValueSerializer(String valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public String getKafkaServers() {
        return kafkaServers;
    }

    public void setKafkaServers(String kafkaServers) {
        this.kafkaServers = kafkaServers;
    }
}

package com.joysuccess.common.utils;

/**
 * 返回json格式的数据
 *
 * @author zhangqing
 * @date 2016年11月22日
 */
public class ResponseJson implements java.io.Serializable{
    private static final long serialVersionUID = 1L;

    public ResponseJson(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResponseJson(int status, String message, Object obj) {
        this.status = status;
        this.message = message;
        this.obj = obj;
    }

    /**状态码*/
    private int status;

    /** 显示信息*/
    private String message;

    /** 数据体*/
    private Object obj = null;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}

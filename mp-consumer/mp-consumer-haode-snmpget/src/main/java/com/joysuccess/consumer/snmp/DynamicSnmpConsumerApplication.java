package com.joysuccess.consumer.snmp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : zhangqing
 * @Description: 动环数据被kafka消费者消费
 * @date : 2020年05月29日 15:44
 */
@SpringBootApplication
public class DynamicSnmpConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DynamicSnmpConsumerApplication.class,args);
    }
}

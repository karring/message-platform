package com.joysuccess.alarms.stream;//package com.joysuccess.consumer.stream;
//
//import com.joysuccess.consumer.redis.producers.RedisSerivce;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.streams.StreamsBuilder;
//import org.apache.kafka.streams.kstream.KStream;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//
///**
// * 数据流
// *
// * @author zhangqing
// * @date 2019年05月06日
// */
//@Component
//@Slf4j
//public class CollectionPointStream {
//    @Resource
//    private RedisSerivce redisSerivce;
//    @Bean
//    KStream<Object, Object> stream(StreamsBuilder streamBuilder){
//        final KStream<Object, Object> redisStream = streamBuilder.stream("collection_points");
//        redisStream.foreach((key,value)-> redisSerivce.saveMessage(value));
//        //redisStream.to("");
//        return redisStream;
//    }
//}

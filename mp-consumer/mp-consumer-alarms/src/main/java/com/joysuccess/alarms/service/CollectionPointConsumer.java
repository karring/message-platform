package com.joysuccess.alarms.service;

import com.alibaba.fastjson.JSONObject;
import com.joysuccess.alarms.mapper.SysAlarmMapper;
import com.joysuccess.alarms.model.Alarm;
import com.joysuccess.alarms.model.SysAlarm;
import com.joysuccess.common.utils.HelpUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

/**
 * 告警消费者。
 *
 * @author zhangqing
 * @date 2019年05月04日
 */
@Slf4j
@Component
public class CollectionPointConsumer {

    private final static Logger LOGGER = LoggerFactory.getLogger(CollectionPointConsumer.class);

    @Autowired
    private SysAlarmMapper sysAlarmMapper;

    @Autowired
    private OkHttpClient okHttpClient;

    /**
     * 动环告警
     * @param record
     */
    @KafkaListener(topics = "alarm-messages")
    public void onMessageDynamicAlarm(ConsumerRecord<?, String> record) {
        Alarm alarmMessage = JSONObject.parseObject(record.value(), Alarm.class);
        SysAlarm sysAlarm = new SysAlarm();
        sysAlarm.setId(HelpUtils.getUUID());
        sysAlarm.setContent(alarmMessage.getAlarmDescription());
        sysAlarm.setLevel(alarmMessage.getAlarmLevel());
        sysAlarm.setPointCode(alarmMessage.getPointId());
        sysAlarm.setStartDate(alarmMessage.getAlarmTime());
        sysAlarm.setStatus(alarmMessage.getAlarmsStatus());
        sysAlarm.setType("0A139B87279041AFA294363848D80DA0");
        sysAlarm.setActiveFlag(1);
        sysAlarm.setCreateBy("admin");
        sysAlarm.setCreateDate(new Date());
        sysAlarmMapper.insert(sysAlarm);
    }

    /**
     * 冷源群控告警
     * @param record
     */
    @KafkaListener(topics = "lengyuan-bacnet-datas",groupId = "bacnetAlarmGroup")
    public void onMessageBacnetAlarm(ConsumerRecord<?, String> record) {
        String value = record.value();
        JSONObject jsonObject = JSONObject.parseObject(value);

        String objectName = (String)jsonObject.get("objectName");
        String presentValue = (String)jsonObject.get("presentValue");
        //以-F结尾的都是告警信息
        if(objectName.endsWith("-F")){
//            SysAlarm sysAlarm = new SysAlarm();
//            sysAlarm.setId(HelpUtils.getUUID());
//            sysAlarm.setContent(presentValue);
//            sysAlarm.setLevel(2);
//            sysAlarm.setStatus(1);
//            sysAlarm.setPointCode(objectName);
//            sysAlarm.setStartDate(new Date());
//            sysAlarm.setType("0B4F668830534C5B8ED4A23B2BDFCCF1");
//            sysAlarm.setActiveFlag(1);
//            sysAlarm.setCreateBy("admin");
//            sysAlarm.setCreateDate(new Date());
//            sysAlarmMapper.insert(sysAlarm);

            String url = "http://localhost:8888/";
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody requestBody = RequestBody.create(mediaType, jsonObject.toJSONString());
            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();
            Response response = null;
            try {
                response = okHttpClient.newCall(request).execute();
                if(!response.isSuccessful()) {
                    LOGGER.error("---冷源群控 发送告警失败！！");
                    throw new IOException("冷源群控 发送告警失败");
                } else {
                    LOGGER.info("冷源群控 发送告警成功");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(null != response) {
                    response.close();
                }
            }
        }
    }
}

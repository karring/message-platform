package com.joysuccess.alarms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joysuccess.alarms.model.SysAlarm;


public interface SysAlarmMapper extends BaseMapper<SysAlarm> {
}

package com.joysuccess.alarms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 告警处理
 * @author zhangqing
 * @date 2019年05月31日
 */
@SpringBootApplication
@MapperScan("com.joysuccess.alarms.mapper")
public class AlarmsApplication {
    public static void main(String[] args) {
        SpringApplication.run(AlarmsApplication.class, args);
    }
}

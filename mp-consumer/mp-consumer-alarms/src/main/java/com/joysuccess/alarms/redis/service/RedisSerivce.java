package com.joysuccess.alarms.redis.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Redis操作接口
 *
 * @author zhangqing
 * @date 2019年05月06日
 */
@Service
@Slf4j
public class RedisSerivce {
    public void saveMessage(Object value){
       log.info(value.toString());
    }
}

package com.joysuccess.alarms.config;//package com.joysuccess.consumer.config;
//
//import org.apache.kafka.streams.StreamsConfig;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.annotation.EnableKafkaStreams;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 消费者重新配置
// *
// * @author zhangqing
// * @date 2019年05月04日
// */
//@Configuration
//@EnableKafkaStreams
//public class KafkaStreamConfig {
//    @Value("${spring.kafka.streams.application-id}")
//    private String applicationId;
//    @Value("${spring.kafka.bootstrap-servers}")
//    private String servers;
//    @Value("${spring.kafka.consumer.key-deserializer}")
//    private String keyDeserializer;
//    @Value("${spring.kafka.consumer.value-deserializer}")
//    private String valueDeserializer;
//
//    @Bean
//    public StreamsConfig kStreamsConfigs() {
//        Map<String, Object> props = new HashMap<>();
//        props.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
//        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, servers);
//        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, keyDeserializer);
//        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, valueDeserializer);
//        return new StreamsConfig(props);
//    }
//}

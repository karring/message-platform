package com.joysuccess.alarms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.joysuccess.alarms.model.SmCollectionPoint;


public interface CollectionPointMapper extends BaseMapper<SmCollectionPoint> {
}

package com.joysuccess.consumer.snmp.model;

/**
 * 实时采集值推送封装格式
 *
 * @author shenchao
 * @date 2017/10/14
 */
public class CollectedData {

    private String pointOrigin;//采集点来源
    private String pointCode;    //采集点编码
    private String pointValue;   //采集点数值
    private Long pointDate;    //采集时间

    public String getPointOrigin() {
        return pointOrigin;
    }

    public void setPointOrigin(String pointOrigin) {
        this.pointOrigin = pointOrigin;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getPointValue() {
        return pointValue;
    }

    public void setPointValue(String pointValue) {
        this.pointValue = pointValue;
    }

    public Long getPointDate() {
        return pointDate;
    }

    public void setPointDate(Long pointDate) {
        this.pointDate = pointDate;
    }
}

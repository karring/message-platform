package com.joysuccess.consumer.snmp.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "sm_collection_point")
@Data
public class SmCollectionPoint {
    /**
     * 活跃的，及没有删除的记录
     */
    public static final Integer ACTIVE_FLAG_YES = 1;

    /**
     * 不活跃的，及删除的记录
     */
    public static final Integer ACTIVE_FLAG_NO = 0;

    /**
     * 主键
     */
    @Id
    @Column(name = "ID",length = 32)
    protected String id;

    /**
     * 删除标识
     */
    @Column(name = "ACTIVE_FLAG")
    protected Integer activeFlag;

    /**
     * 创建人
     */
    @Column(length = 32, name = "CREATE_BY")
    protected String createBy;

    /**
     * 创建时间 - 时间戳
     */
    @Column(name = "CREATE_DATE")
    protected Date createDate;

    /**
     * 修改人
     */
    @Column(length = 32, name = "UPDATE_BY")
    protected String updateBy;

    /**采集点来源 */
    @Column(name="POINT_ORIGIN")
    private String pointOrigin;

    /**采集点名称 */
    @Column(name="POINT_NAME")
    private String pointName;

    /**采集点编码 */
    @Column(name="POINT_CODE")
    private String pointCode;

    /**类型 */
    @Column(name="POINT_TYPE")
    private String pointType;

    /**采集值 */
    @Column(name="POINT_VALUE")
    private String pointValue;

    /**采集时间 */
    @Column(name="POINT_DATE")
    private Date pointDate;

    /**告警类型 */
    @Column(name="ALARM_TYPE")
    private String alarmType;

    /**永久显示  0 否  1 是 */
    @Column(name="IS_SHOW")
    private String isShow;

    /**是否告警 0 否  1 是 */
    @Column(name="IS_ALARM")
    private String isAlarm;

    /**所在设备ID */
    @Column(name="ASSERT_ID")
    private String assertId;

    /**备注 */
    @Column(name="REMARK")
    private String remark;

    /**阀值上限 */
    @Column(name="THRESHOLD_UP1")
    private Double thresholdUp1;

    /**阀值下限 */
    @Column(name="THRESHOLD_DOWN1")
    private Double thresholdDown1;

    /**阀值上限 */
    @Column(name="THRESHOLD_UP2")
    private Double thresholdUp2;

    /**阀值下限 */
    @Column(name="THRESHOLD_DOWN2")
    private Double thresholdDown2;

    /**阀值上限 */
    @Column(name="THRESHOLD_UP3")
    private Double thresholdUp3;

    /**阀值下限 */
    @Column(name="THRESHOLD_DOWN3")
    private Double thresholdDown3;

    /**阀值上限 */
    @Column(name="THRESHOLD_UP4")
    private Double thresholdUp4;

    /**阀值下限 */
    @Column(name="THRESHOLD_DOWN4")
    private Double thresholdDown4;

    /**阀值上限 */
    @Column(name="THRESHOLD_UP5")
    private Double thresholdUp5;

    /**阀值下限 */
    @Column(name="THRESHOLD_DOWN5")
    private Double thresholdDown5;

    /**阀值上限 */
    @Column(name="THRESHOLD_UP6")
    private Double thresholdUp6;

    /**阀值下限 */
    @Column(name="THRESHOLD_DOWN6")
    private Double thresholdDown6;
}
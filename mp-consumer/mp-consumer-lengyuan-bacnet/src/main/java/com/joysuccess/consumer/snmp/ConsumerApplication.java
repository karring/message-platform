package com.joysuccess.consumer.snmp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 消费者
 *
 * @author zhangqing
 * @date 2019年05月31日
 */
@SpringBootApplication
//@MapperScan("com.joysuccess.consumer.mapper")
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }
}

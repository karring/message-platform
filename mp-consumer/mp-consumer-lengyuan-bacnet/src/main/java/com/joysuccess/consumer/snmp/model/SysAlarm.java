package com.joysuccess.consumer.snmp.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Table(name = "SYS_ALARM")
public class SysAlarm extends BaseEntity {

    /** 告警类型*/
    @Column(name="POINT_ORIGIN")
    private String pointOrigin;

    /** 告警类型*/
    @Column(name="TYPE")
    private String type;

    /** 告警内容*/
    @Column(name="CONTENT")
    private String content;

    /** 告警级别*/
    @Column(name="LEVEL")
    private Integer level;

    /** 告警状态*/
    @Column(name="STATUS")
    private Integer status;


    /** 告警资产ID*/
    @Column(name="TYPE_ID")
    private String typeId;

    /** 采集点编码*/
    @Column(name="POINT_CODE")
    private String pointCode;

    /** 其他信息id*/
    @Column(name="OTHER_INFO")
    private String otherInfo;

    /** 告警开始时间*/
    @Column(name="START_DATE")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startDate;

    /** 解除人*/
    @Column(name="RELEASE_BY")
    private String releaseBy;

    /** 告警结束时间*/
    @Column(name="RELEASE_DATE")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date releaseDate;

    @Transient
    private String position;

    @Transient
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date startDate2;

    @Transient
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date releaseDate2;

    public String getPointOrigin() {
        return pointOrigin;
    }

    public void setPointOrigin(String pointOrigin) {
        this.pointOrigin = pointOrigin;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId == null ? null : typeId.trim();
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getReleaseBy() {
        return releaseBy;
    }

    public void setReleaseBy(String releaseBy) {
        this.releaseBy = releaseBy;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getPosition() {return position;}

    public void setPosition(String position) {this.position = position == null ? null : position.trim();}

    public Date getStartDate2() {
        return startDate2;
    }

    public void setStartDate2(Date startDate2) {
        this.startDate2 = startDate2;
    }

    public Date getReleaseDate2() {
        return releaseDate2;
    }

    public void setReleaseDate2(Date releaseDate2) {
        this.releaseDate2 = releaseDate2;
    }
    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    @Override
    public String toString() {
        return "SysAlarm{" +
                "id='"+getId()+'\''+
                "type='" + type + '\'' +
                ", content='" + content + '\'' +
                ", level=" + level +
                ", status=" + status +
                ", typeId='" + typeId + '\'' +
                ", pointCode='" + pointCode + '\'' +
                ", startDate=" + startDate +
                ", releaseDate=" + releaseDate +
                '}';
    }

}
package com.joysuccess.consumer.mina;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 消费者
 *
 * @author zhangqing
 * @date 2019年05月31日
 */
@SpringBootApplication
@MapperScan("com.joysuccess.consumer.mina.mapper")
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }
}

package com.joysuccess.thread;


import redis.clients.jedis.Jedis;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : zhangqing
 * @Description: 线程创建工厂
 * @date : 2020年06月22日 10:51
 */
public class JoyNameThreadFactory implements ThreadFactory {

    private final static String THREAD_PREFIX = "monitor-platform-";

    AtomicInteger atomicInteger = new AtomicInteger(0);

    /**
     * 分布式全局唯一主键
     * @return
     */
    private long getDistributeId(){
        Jedis jedis = new Jedis("127.0.0.1");
        long threadDistributeId;
        try {
            threadDistributeId  = jedis.incr("threadDistributeId");
        }finally {
            jedis.close();
        }
        return threadDistributeId;
    }

    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(r, THREAD_PREFIX + atomicInteger.getAndIncrement());
        return t;
    }
}

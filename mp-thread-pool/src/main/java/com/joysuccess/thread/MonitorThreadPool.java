package com.joysuccess.thread;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author : zhangqing
 * @Description: 监控线程池状态
 * @date : 2020年06月22日 12:35
 */
public class MonitorThreadPool {

    private int activeCount;
    private int corePoolSize;
    private int maximumPoolSize;


    public void MonitorThreadPool(){
        /**
         * 获取线程池执行者
         */
        JoyThreadPool joyThreadPool = JoyThreadPool.getJoyThreadPool();
        ThreadPoolExecutor threadPoolExecutor = joyThreadPool.getThreadPoolExecutor();


        activeCount = threadPoolExecutor.getActiveCount();
        corePoolSize = threadPoolExecutor.getCorePoolSize();
        maximumPoolSize = threadPoolExecutor.getMaximumPoolSize();
    }


}

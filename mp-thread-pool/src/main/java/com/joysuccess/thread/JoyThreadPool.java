package com.joysuccess.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : zhangqing
 * @Description: 线程池
 * @date : 2020年06月22日 10:53
 */
public class JoyThreadPool {

    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 单例模式，启动加载线程池
     */
    private static JoyThreadPool joyThreadPool = new JoyThreadPool();

    public static JoyThreadPool getJoyThreadPool(){

        return joyThreadPool;
    }

    public ThreadPoolExecutor getThreadPoolExecutor(){
        return threadPoolExecutor;
    }


    /**
     * 初始化线程池
     * @return
     */
    private JoyThreadPool(){
        /* 核心线程池的大小 */
        int corePoolSize = 100;
        /* 核心线程池的最大线程数 */
        int maxPoolSize = 200;
        /* 线程最大空闲时间 */
        long keepAliveTime = 60;
        /* 时间单位 */
        TimeUnit unit = TimeUnit.SECONDS;
        /* 阻塞队列 容量为2 */
        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(200);
        /* 线程创建工厂 */
        ThreadFactory threadFactory = new JoyNameThreadFactory();
        /* 线程池拒绝策略 */
        RejectedExecutionHandler handler = new JoyIgnorePolicy();
        ThreadPoolExecutor executor = null;
        /* 推荐的创建线程池的方式 */
        /* 不推荐使用现成的API创建线程池 */
        executor = new ThreadPoolExecutor(corePoolSize,
                maxPoolSize, keepAliveTime, unit,
                workQueue, threadFactory, handler);
        /* 预启动所有核心线程  提升效率 */
        executor.prestartAllCoreThreads();
        this.threadPoolExecutor = executor;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        AtomicInteger atomicInteger = new AtomicInteger(0);
        JoyThreadPool joyThreadPool = new JoyThreadPool();
        ThreadPoolExecutor threadPoolExecutor = joyThreadPool.getThreadPoolExecutor();
        List<Future> futureList = new ArrayList<>();


        while (true) {
            RunnableTask task = new RunnableTask(String.valueOf(atomicInteger.getAndIncrement()));
            threadPoolExecutor.execute(task);
        }
//
//        while (true) {
//            RunnableTask task = new RunnableTask(String.valueOf(atomicInteger.getAndIncrement()));
//            Future future = threadPoolExecutor.submit(task);
//            futureList.add(future);
//
//            if(futureList.size() == 5){
//                writeData(futureList);
//                futureList.clear();
//            }
//        }
    }

    public static void writeData(List<Future> futureList){
        for (int i = 0; i < futureList.size(); i++) {
            try {
                futureList.get(i).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 线程
     */
    static class RunnableTask implements Runnable {
        private String name;

        public RunnableTask(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println(this.toString() + " is running!");
            try {

                //让任务执行慢点,主要是为了让线程达到最大值
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return "RunnableTask [name=" + name + "]";
        }
    }
}
